<?php
    $maskId = null;
    if(isset($_GET['maskId']))
    {
        $maskId = $_GET['maskId']; 
        $titleMessage = "Some property name will be here.";
        
    }
?>

<!doctype html>
<html ng-app="chatApp">
    <head>
        <title>CHAT</title>
        <link href="scripts/jquery/jquery-ui.css" rel="stylesheet">
        <script src="scripts/jquery/external/jquery/jquery.js"></script>
        <script src="scripts/jquery/jquery-ui.js"></script>
        <script src="scripts/chat.js"></script>
        <script src="scripts/service.js"></script>
        <link href="scripts/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="//media.twiliocdn.com/sdk/js/client/v1.3/twilio.min.js"></script>
    	<script src="//media.twiliocdn.com/taskrouter/js/v1.7/taskrouter.min.js"></script>
	    <script src="https://media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.js"></script>
	    <script src="https://media.twiliocdn.com/sdk/rtc/js/ip-messaging/v0.10/twilio-ip-messaging.js"></script>
        <style>
            .ui-dialog-titlebar-close {
             visibility: hidden;
            }
            
      </style>
    </head>
<body>
    <form>
<div id="dialogonator">
        <div id="messageHistory" style="overflow: scroll;height:250px;">
            <div class="input-group" id="chatdialog_events" >
            </div>
        </div>
            </br>
            <div class="input-group" id="chatdialog" style="border-top:1px solid #ccc">
                <span class="input-group-addon" id="basic-addon3">Message</span>
                <textarea id="chat_Message"  placeholder="message" cols="36" rows="4"/></textarea>
            </div>
            <div style="padding-top:4px;" class="btn pull-right" >
                <button id="chat_button" class="ui-button ui-corner-all ui-widget">
                    <span class="glyphicon glyphicon-phone"></span>Send
                </button>
            </div>
    </div>
        <script>
        $("#dialogonator" ).dialog({
        autoOpen: true,
        width: 500,
        height:450});
        $( document ).ready(function() {
            console.log( "ready!" );
            service.setTheTitles('<?=$maskId?>');
        });
        </script>
</form>
</body>
</html>