var service = {}

service.CreateCustomer = function(params)
{
     
    var prospect = "";
    var prospectData = null;
    $.ajax({
        type: "POST",
        url: ajax_app_path + '/message/prechathandler',
        data: params,
        async: false,
        dataType: "json",
        success: function (msg) {
            //alert("Here");
            prospect = msg;
            console.log(JSON.stringify(prospect));
            //object you will be getting back is {"maskid":"d302ebb0-23b4-4014-9403-e4a43164cef1","chatEventId":123456789,"prospectName":"Testy McTester","skill":"Leasing"}
            
                //TODO validate that the prospect has the required fields. If null then
                if(prospect !== null)
                {   
                    chat.connect(prospect);
                    //show a dialog and let the prospect know we are having tech difficulty.
                    $("#dialogonator").dialog('open'); 
                    //hide the other layer
                    $("#dialogInit").dialog("close");
                }else{
                    //gracefully handle this

                }            
        },
        error: function (msg) {
            $("#dialogonator").dialog('close'); 
            $("#dialogInit").dialog("close");
            //alert(requiredMessage);
            $('#errorMessage').dialog("option", "title", 'Error');
            $('#errorMessage').html(JSON.stringify(msg));
            $("#errorMessage").dialog('open');  
            $('#errorMessage').dialog({ buttons: [
            {
                text: "Close",
                click: function () { $(this).dialog("close");}
            }
            ]});  
            return;
        }
    });    

    return prospectData;

}
/* deprecated
service.CreateCustomerSms = function (params)
{
    alert("I got called");
    return;
     
    var prospect = "";
    $.ajax({
        type: "POST",
        url: ajax_app_path + '/message/prechathandler',
        data: params,
        async: false,
        dataType: "json",
        success: function (msg) {
            prospect = msg;
            console.log(JSON.stringify(prospect));
            
            
        },
        error: function (msg) {
            //alert("Error " + JSON.stringify(msg));
            //need to send this out to the erorr handler?Possibly slack?
        }
    });    

    return prospectData;

}
*/

service.checkemail = function (emailAddress){
    var testresults  = false;
    var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    if (filter.test(emailAddress))
        testresults=true
    else{
        teststresults=false
    }
    return (testresults)
}

service.checkphone = function (phone){
    var testresults  = false;
    var filter=/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
    if (filter.test(phone))
        testresults=true
    else{
        teststresults=false
    }
    return (testresults)
}

service.SubmitForm = function (maskid)
{
    var prospect;
   
    $("#chat_button").hide();
    //get all the fields that have an attribute of dynaform
    var canContinue = true;
    var requiredMessage = "Fields Highlighted in red are required.";
    var param = {maskid: maskid};
    $('[surveyfield]').each(function(index) {
        $(this).attr("style", "");
        if($(this).attr( "required" ) === "required")
        {
            if(($(this).val() === null) || ($(this).val().length === 0))
            {
                $(this).attr("style", "border:2px dashed red;");
                canContinue = false;
            }else{
                //lets check to see if this has the word email address in it
                if( $(this).attr( "id" ).match(/(email)/gi)){
                    //this passed. Validate that we have a proper email address
                    if(!service.checkemail($(this).val())){
                        requiredMessage = $(this).val() + " is an invalid email address";
                        $(this).attr("style", "border:2px dashed red;");
                        $(this).focus();
                        canContinue = false;
                    }
                }//email checker

                //lets check to see if there is a field called phone
                if( $(this).attr( "id" ).match(/(phone)/gi)){
                    //this passed. Validate that we have a proper email address
                    if(!service.checkphone($(this).val())){
                        requiredMessage = $(this).val() + " is an invalid phone number";
                        $(this).attr("style", "border:2px dashed red;");
                        $(this).focus();
                        canContinue = false;
                    }
                }//email checker

                param[$(this).attr( "id" )] = $(this).val();                            
            }
        }
    });

    if(!canContinue)
    {
        //alert(requiredMessage);
        $('#errorMessage').dialog("option", "title", 'Required fields missing');
        $('#errorMessage').html(requiredMessage);
        $("#errorMessage").dialog('open');  
        $('#errorMessage').dialog({ buttons: [
        {
            text: "Close",
            click: function () { $(this).dialog("close");}
        }
        ]});  
        return;

    }else{
            console.log(JSON.stringify(param));     
            prospect = service.CreateCustomer(param);
            

    }
    return false;
}

