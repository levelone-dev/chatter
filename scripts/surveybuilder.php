<?php
require_once "vendor/autoload.php"; // Loads the library

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;


class surveybuilder 
{  
    public $stylesheet = "";

    function buildsurvey($maskid) { 
        if(!$this->agentsavailable($maskid))
        {
            print_r("No agents available");
            die();
        }
        $client = new \GuzzleHttp\Client();
        $html = "";
        $url = "http://localhost/api/survey/request/" . $maskid;
        //$client = new \GuzzleHttp\Client();
        try {
            $response = $client->get($url);
        }
        catch (ClientException $e) {
            print_r("maskId doesn't exist.");
            die();
        }
        catch (Exception $e) {
            print_r("maskId doesn't exist.");
            die();
        }

        $data = json_decode($response->getBody()->getContents(), true);
        $this->stylesheet = '<link rel="stylesheet" href="' . $data["surveyDetails"][0]["styleSheetUrl"] . '" type="text/css" />';
      
        //$m = new Mustache_Engine;
        foreach($data["formFields"] as $obj)
        {
            if($obj["fieldType"] === "Text")
            {   
                $html .= $this->settext($obj);
            }
            elseif($obj["fieldType"] === "DropDown")
            {
                $html .= $this->setselect($obj);
            }
            elseif($obj["fieldType"] === "RadioButton")
            {
                $html .= $this->setradio($obj);
            }
            elseif($obj["fieldType"] === "CheckBox")
            {
                $html .= $this->setcheckbox($obj);
            }
        }
        $html .= $this->addbutton($maskid);
        return $html;
    }
    function surveydetails($maskid)
    {
        try {
        $url = 'http://localhost/api/mask/' . $maskid . '/partner';
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url);
        }
        catch(ServerException $e) {
            print_r("MaskId doesn't exist");
            die();
        }
        $partner = json_decode($response->getBody()->getContents(), true);
        $partnername = $partner["partner"]["friendlyName"];
        return $partnername;
    }
    function setselect($obj)
    {   
        $url = 'http://localhost/api/fieldvalues/'. $obj["id"] .'/get';
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url);
        $fieldvalues = json_decode($response->getBody()->getContents(), true);
        $html = "<div class=\"form-group\">" .
                "<label>" . $obj["friendlyName"] . "</label> " .
                "<select class=\"selectpicker\" id=" . $obj["fieldName"] . " name=" . $obj["fieldName"] . " surveyfield=\"true\" required='required'>" .
                "<option class=\"form-control\" value=\"null\"></option>";
               
                for($i=0; $i<=count($fieldvalues); $i++)
                {   
                    $html .= "<option class=\"form-control\" value=" . $fieldvalues["fieldValues"][$i]["fieldValue"] . ">" . $fieldvalues["fieldValues"][$i]["fieldValue"] . "</option>";
                }
                $html .= "</select>" .
                "</div>";        
        return $html;
    }
    function settext($obj)
    {
        $html = "<div class=\"form-group\">" . 
                "<label id=\"lbl". $obj["fieldName"] ."\">" . $obj["friendlyName"] . "</label>" . 
                "<input type=\"text\" name=" . $obj["fieldName"] . " id="  . $obj["fieldName"] . " class=\"form-control\" aria-describedby=\"basic-addon1\" surveyfield=" . $obj["fieldName"] . " required='required' placeholder=". $obj["friendlyName"] .">" .
                "</div>";
        return $html;
    }
    function setradio($obj)
    {   
        $url = 'http://localhost/api/fieldvalues/'. $obj["id"] .'/get';
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url);
        $fieldvalues = json_decode($response->getBody()->getContents(), true);
        $html = "<div class=\"form-group\">" .
                "<label>" . $obj["friendlyName"] . "</label>";
                for($i=0; $i<=count($fieldvalues); $i++)
                {   
                    $html .= "<div class=\"radio\">" .
                    "<label><input type=\"radio\" id=" . $obj["fieldName"] . " name=" . $obj["fieldName"] . " value=" . $fieldvalues["fieldValues"][$i]["fieldValue"] ." surveyfield=\"true\"  required='required'>" . $fieldvalues["fieldValues"][$i]["fieldValue"] . "</label>" .
                    "</div>";
                }
                $html .= "</div>";
        return $html;
    }
    function setcheckbox($obj)
    {   
        $url = 'http://localhost/api/fieldvalues/'. $obj["id"] .'/get';
        $client = new \GuzzleHttp\Client();
        $response = $client->get($url);
        $fieldvalues = json_decode($response->getBody()->getContents(), true);
        $html = "<div class=\"form-group\">" .
                "<label>" . $obj["friendlyName"] . "</label>";
                for($i=0; $i<=count($fieldvalues); $i++)
                {
                    $html .= "<div class=\"checkbox\">" .
                    "<label><input type=\"checkbox\" id=" . $obj["fieldName"] . " name=" . $obj["fieldName"] . " value=" . $fieldvalues["fieldValues"][$i]["fieldValue"] ." surveyfield=\"true\" required='required'>" . $fieldvalues["fieldValues"][$i]["fieldValue"] . "</label>" .
                    "</div>";
                }
            $html .= "</div>";
        return $html;
    }
    function addbutton($maskid)
    {
        $html = "<div class=\"form-group\">" .
            "<div style=\"padding-top:4px;\" class=\"btn pull-right\">" .
            "<button type=\"submit\" id=\"agent_connect\" class=\"ui-button ui-corner-all ui-widget\" onClick=\"service.SubmitForm('" . $maskid ."')\">" .
            "<span class=\"glyphicon glyphicon-phone\"></span>Connect with an Agent" .
            "</button>" .
            "</div>";
        return $html;
    }
    function agentsavailable($maskid)
    {
        // grab a list of workers based on skillset. Then loop through them and get thier availablity
        $client = new \GuzzleHttp\Client();
        $html = "";
        $url = "http://localhost/api/agent/available";
        //$client = new \GuzzleHttp\Client();
        $response = $client->get($url);
        $data = json_decode($response->getBody()->getContents(), true);        
        return $data;
    }
}