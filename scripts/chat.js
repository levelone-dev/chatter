var chat = {}

    var i = 0;
    var prospectName = "";
    var thisChannel;

    chat.printMessage = function (fromUser, message, log) {
            i ++;
            
            if(fromUser == prospectName)
            {
                $("#chatdialog_events").append('<div class="alert alert-warning" id="div_' + i + '">'+fromUser + " : " + message+'</div>');  
                
            }else {
                $("#chatdialog_events").append('<div class="alert alert-success" id="div_' + i + '">'+fromUser + " : " + message+'</div>');
            }
            //log message
            // if(log) {
            //     var maskid = thisChannel.attributes.maskId
            //     var partnerid = '857'; 
            //     var chateventid = thisChannel.attributes.chatEventId
            //     var content = message; 
            //     var direction = '1';
            //     var agentid = '0';
            //     var params = {'maskid': maskid, 'partnerid': partnerid, 'chateventid':  chateventid,  'content' : content, 'direction' : direction, "agentid" : agentid}

            //     $.ajax({
            //         type: "GET",
            //         url: ajax_app_path + '/api/message/addevent',
            //         async: false,
            //         data: params,
            //         dataType: "json",
            //         success: function (msg) {
            //             console.log("success logging");
            //         },
            //         error: function (msg) {
            //             console.log("Error " + msg.responseText);
            //         }
            //     });
            // $("#div_" + i).html();
            // $("#div_" + i).focus();
            // window.location.hash = "#div_" + i;
            // }
            ("#div_" + i).html();
            $("#div_" + i).focus();
            window.location.hash = "#div_" + i;
    }
    chat.setupChannel = function (thisChannel) {
        // Join the channel
        thisChannel.join().then(function(channel) {
            chat.printMessage($('#title').html()," will be with you shortly", false);
        });
        thisChannel.on('messageAdded', function(message) {
            chat.printMessage(message.author, message.body, true);
        });
    }
    chat.connect = function (prospect) {
            var accessManager;
            var messagingClient;
            var tokenData;
            var channelsid;
            /* commented out to add the route channel */
            
            params = {
                name: prospect.prospectName,
                device: "browser"
            }
            prospectName = prospect.prospectName;

            $.ajax({
                type: "GET",
                url: ajax_app_path + '/api/token/init',
                data: params,
                dataType: "text",
                async: false,  
                success: function (data) {
                    token = data;
                },
                error: function (msg) {
                    alert("Error " + msg);
                }

            });
            params = {
                uniqueName: prospect.skill + "_" + prospect.chatEventId, //prospect.route_channel
                friendlyName: prospect.skill + "_" + prospect.chatEventId,
                attributes: {'chatEventId': prospect.chatEventId, 'maskId' : prospect.maskid }
            }

            $.ajax({
                type: "GET",
                url: ajax_app_path + '/api/channel/init',
                data: params,
                async: false,
                dataType: "text",
                success: function (data) {
                    channelsid = data;
                    console.log(channelsid);
                },
                error: function (msg) {
                    // alert("Error " + msg);
                }
            });  
            
            params = {
                From: prospect.prospectName,
                channel: channelsid,
                skill: prospect.skill,
                chatEventId: prospect.chatEventId,
                maskId: prospect.maskid,
                route_channel: prospect.skill + "_" + prospect.chatEventId
            }
            //additional info here
            //Check  the task.php add additional parameters.
            $.ajax({
                type: "GET",
                url: ajax_app_path + '/api/task/init',
                data: params,
                async: false,
                dataType: "text",
                success: function (data) {
                    
                },
                error: function (msg) {
                    console.log("Error " + msg.responseText);
                }
            }); 
        console.log(token);    
        let chatClient = new Twilio.Chat.Client(token);
        chatClient.initialize()
        .then(() => {
            var promise = chatClient.getChannelBySid(channelsid);
            promise.then(function(channel) {
                console.log('channel is: ' + channel.uniqueName);
                thisChannel = channel;
			    chat.setupChannel(thisChannel);
                })
            });    


            // Send a new message to the general channel
            var $input = $('#chat_Message');
            $input.on('keydown', function(e) {
                if (e.keyCode == 13) {
                    //Send the message to the logger.
                    thisChannel.sendMessage($input.val())
                    $input.val('');
                }
            });
}       
   


