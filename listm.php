<?php
// This line loads the library
require_once "vendor/autoload.php";

use Twilio\Rest\Client;

// Find your Account Sid and Token at twilio.com/user/account
$sid = "AC5fb281d5af135a8f2ece46790cbaa2cd";
$token = "0be5106e4ab0a07ccf16d56fc8d4bd5a";
$workspaceSid="";

// Initialize the client
$client = new Client($sid, $token);

//Retrieve the messages
$messages = $client->ipMessaging
    ->services("IS25a8ee92a10141baa80fea0f2f71f22d")
    ->channels("CH9106ae737f80446ebe82f01d372ec451")
    ->messages
    ->read();

//List the messages
foreach ($messages as $message) {
    echo $message->to . ' - ' . $message->body . ' - ' . $message->from   . PHP_EOL;
}

//  $members = $client->ipMessaging
//     ->services("IS25a8ee92a10141baa80fea0f2f71f22d")
//     ->channels("CH56576ea6736b4b70a0d14eac67ccb150")
//             ->members
//             ->read();
//             foreach($members as $member) {
//                echo($member->identity . PHP_EOL);
//                 }
            