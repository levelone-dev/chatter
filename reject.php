<?php
// Get the PHP helper library from twilio.com/docs/php/install 
require_once '/path/to/vendor/autoload.php'; // Loads the library 
 
use Twilio\Rest\Client; 
 
$account_sid = 'ACbbeccabddca72405042046a7a7cc1310'; 
$auth_token = '[AuthToken]'; 
$client = new Client($account_sid, $auth_token); 
 
$reservations = $client->taskrouter->v1->workspaces('WS33badc280d92b91b61e0a8255b95f5ac') 
  ->tasks('$tasksid') 
  ->reservations('$reservationsid')->update(array(  
      'WorkerActivitySid' => 'WA97011fb669ee382485ffa0b81f86819f', 
      'Instruction' => 'Reject'               
  )); 