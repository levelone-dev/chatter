<?php
require_once __DIR__ .'/scripts/surveybuilder.php';

    
 if(isset($_GET['maskid']))
    {
        $maskId = $_GET['maskid']; 
        $titleMessage = "Some property name will be here.";
    }
    

    $s = new surveybuilder();
    $partnername = $s->surveydetails($maskId);
    $html = $s->buildsurvey($maskId);
?>
<!doctype html>
<html ng-app="chatApp">
    <head>
        <title id="title"><?=$partnername?></title>
        <!-- Bootstrap -->
        <link href="scripts/jquery/jquery-ui.css" rel="stylesheet">
        <link href="scripts/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="scripts/jquery/external/jquery/jquery.js"></script>
        <script src="scripts/jquery/jquery-ui.js"></script>
        <script src="//media.twiliocdn.com/taskrouter/js/v1.7.1/taskrouter.min.js"></script>
        <script src="https://media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
        <script src="https://media.twiliocdn.com/sdk/js/chat/v0.12/twilio-chat.min.js"></script>
         <script src="scripts/config.js"></script>
        <script src="scripts/chat.js"></script>
        <script src="scripts/service.js"></script>
        <script type="text/javascript" src="scripts/mustache.js"></script>
        <style>
            .ui-dialog-titlebar-close {
             visibility: hidden;
            }
      </style>
      <?=$s->stylesheet?>
      
    </head>
<body>
    <form name="loginForm">
    <div id="errorMessage" title="required"></div>
    <div id="dialogInit" title="<?=$partnername?>">
     <?= $html?>
    </div>
    <div id="dialogonator" title="<?=$partnername?>">
        <div id="messageHistory" style="overflow: scroll;height:250px;">
            <div class="input-group" id="chatdialog_events" >
                
            </div>
        </div>
            </br>
            <div class="input-group" id="chatdialog" style="border-top:1px solid #ccc">
                <span class="input-group-addon" id="basic-addon3">Message</span>
                <textarea id="chat_Message"  placeholder="message" cols="36" rows="4"/></textarea>
            </div>
            <div style="padding-top:4px;" class="btn pull-right" >
                <button id="chat_button" class="ui-button ui-corner-all ui-widget">
                    <span class="glyphicon glyphicon-phone"></span>Send
                </button>
            </div>
    </div>
    
<script>
    $("#dialogInit" ).dialog({
    autoOpen: true,
	width: 500});

     $("#dialogonator" ).dialog({
	autoOpen: false,
	width: 500,
    height:450});

     $("#errorMessage" ).dialog({
	    autoOpen: false,
	    width: 300});

     $( document ).ready(function() {
            console.log( "ready!" );
        });

</script>

</form>
</body>
</html>