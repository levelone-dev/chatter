<?php

// This line loads the library
require_once "vendor/autoload.php"; // Loads the library
use Twilio\Rest\Client;

// Find your Account Sid and Token at twilio.com/user/account
$sid = "AC5fb281d5af135a8f2ece46790cbaa2cd";
$token = "0be5106e4ab0a07ccf16d56fc8d4bd5a";

// Initialize the client
$client = new Client($sid, $token);

// Delete the channel
$channels = $client->ipMessaging
    ->services("IS25a8ee92a10141baa80fea0f2f71f22d")
    ->channels
    ->read();


// List the channels
foreach ($channels as $channel) {
    echo $channel->delete();
}
